# SG-Miner CICD

---

@title[Gitlab workflow]
@snap[north span-100 headline]
### Gitlab workflow
@snapend    

@snap[west span-30]
![](assets/img/git_flow.png)
@snapend

@snap[midpoint span-30]
![](assets/img/gitlab_flow_2.png)
@snapend

@snap[east span-30]
![](assets/img/giltab_flow_1.png)
@snapend

@snap[south-west template-note text-blue]
https://about.gitlab.com/2014/09/29/gitlab-flow/
@snapend

---
@title[Provisioning / Configuration]

@snap[north span-100 headline]
### SG-Miner deployment
@snapend

@snap[span-100]
@ol[list-bullets-black](false)
- Provisioning - HEAT template - manually
- Configuration - gitlab pipelines - triggered by pushes to build branch
@olend

@snapend

---
@title[Configuration pipelines]

@snap[west span-40]
### Three continuous integration pipelines
@snapend

@snap[north-east span-60 fragment]
@box[bg-blue text-white](Pipeline 1.#Deploy to dev/stage/prod - default pipeline)
@snapend

@snap[east span-60 fragment]
@box[bg-blue text-white](Pipeline 2.#Rebuild VMs and deploy to dev - checks for playbook idempotency)
@snapend

@snap[south-east span-60 fragment]
@box[bg-blue text-white](Pipeline 3.#Only deploy importer scripts - much faster than deploying entire app)
@snapend

---
@title[default pipeline]

@snap[west span-30]
### Default pipeline
@snapend

@snap[east span-80]
![](assets/img/default_pipe_1.png)
![](assets/img/default_pipe_2.png)
@snapend

+++
@title[maven build]

```
maven_build_sgminer_webapp:
  stage: maven_build_sgminer_webapp
  image: maven:latest
  only:
    refs:
    - build
  tags:
  - SCv2-HRZAGT1

  script:
    - git tag --points-at HEAD > deployment_version
    - cd source/sg-miner
    - mvn clean package

  artifacts:
    paths:
    - deployment_version
    - sg-miner-ear.ear
    expire_in: 7 days
```

@[3](Dockher hub image.)
@[4-6](Job triggered only by push to build branch.)
@[7-8](Run by SCv2 gitlab runner)
@[11](If present, get tag and save it in deployment_version file - 1st artifact.)
@[13](Triggers the build process - 2nd artifact.)
@[15-19](Two artifacts produced by the job.)

@snap[north-east text-gray]
maven_build_sgminer_webapp
@snapend

+++
@title[deploy to dev]

```
deploy_dev_instance_without_vm_rebuild:
  stage: deploy_dev_instance_without_vm_rebuild
  image: python:latest
  only:
    refs:
    - build
  except:
  - schedules
  tags:
    - SCv2-HRZAGT1
  dependencies:
    - maven_build_sgminer_webapp

  script:
    # Install gitlab deployer ssh-key
    # https://docs.gitlab.com/ee/ci/ssh_keys/
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SGMINER_CICD_RSA" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-add -l

    # Prepare ansible
    - ls
    - pip install ansible
    - echo $SGMINER_VAULT > /tmp/.vault_pass
    - cd configuration

    # Updating tenants takes hours to complete. Best run overnight. For functionality check import a single tenant
    - chmod 700 .
    - ansible-playbook -i inventories/scv2hrzg/sgminer-dev deploy_sgminer_SCv2.yml --vault-password-file /tmp/.vault_pass --extra-vars "update_tenants='do not update'"

  after_script:
    - rm /tmp/.vault_pass
```

@[3](Dockher hub image.)
@[4-8](Job triggered only by push to build branch.)
@[11-12](Job depends on artifact produced by a previous job.)
@[14-22](Setup the configuration ssh key used to connect to VMs.)
@[24-28](Install ansible and make the vault password available in a file.)
@[30-32](Run the sgminer ansible playbook.)
@[34-35](Clean up secrets.)

@snap[north-east text-gray]
deploy_dev_instance_without_vm_rebuild
@snapend

---
@title[rebuild VM pipeline]

@snap[west span-25]
### Rebuild dev VMs pipeline
@snapend

@snap[east span-75]
![](assets/img/rebuild_pipe_1.png)
![](assets/img/rebuild_pipe_2.png)
@snapend

+++
@title[rebuild dev VMs]

```
rebuild_dev_instance_and_deploy:
  stage: rebuild_dev_instance_and_deploy
  image: python:latest
  only:
    refs:
    - schedules
    variables:
    - $ACTION == "rebuild_dev_instance_and_deploy"
  tags:
    - SCv2-HRZAGT1
  dependencies:
  - maven_build_sgminer_webapp
  variables:
    OS_AUTH_URL: "$OS_AUTH_URL"
    OS_CACERT: "provisioning/pannet.crt"
    OS_TENANT_NAME: "$OS_TENANT_NAME"
    OS_USERNAME: "$OS_USERNAME"
    OS_PASSWORD: "$OS_PASSWORD"

  script:
    # Install gitlab deployer ssh-key
    # https://docs.gitlab.com/ee/ci/ssh_keys/
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SGMINER_CICD_RSA" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-add -l

  # Prepare ansible
    - ls
    - pip install ansible
    - pip install python-openstackclient
    - echo $SGMINER_VAULT > /tmp/.vault_pass

    # Rebuild servers for fresh install test
    - openstack server rebuild sgminer-app
    - openstack server rebuild sgminer-db
    - sleep 5m # wait for rebuild to finish

    # Tenant update is triggered by a cron job overnight...
    - cd configuration
    - chmod 700 .
    - ansible-playbook -i inventories/scv2hrzg/sgminer-dev deploy_sgminer_SCv2.yml --vault-password-file /tmp/.vault_pass --extra-vars "update_tenants='do not update'"

  after_script:
    - rm /tmp/.vault_pass

```

@[4-8](Triggered by schedule. Run daily.)
@[13-18](Need to set credentials for openstack client.)
@[7-8](Run by SCv2 gitlab runner)
@[36-39](Rebuild VMs. Wait some time for rebuild to run and finish.)

@snap[north-east text-gray]
rebuild_dev_instance_and_deploy
@snapend
